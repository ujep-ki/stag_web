<?php

use TechnikTomCZ\StagWeb\Calendar;
use TechnikTomCZ\StagWeb\Repository\DepartureRepository;
use TechnikTomCZ\StagWeb\Repository\SubjectRepository as SubjectRepository;

global $requestParams, $requestCalled, $isSomeParamsSet;

$yearsCount = 5;

if ($requestCalled && $isSomeParamsSet) {
    $response = SubjectRepository::GetData($requestParams);
    $requestRun = true;
} else {
    $response = null;
    $requestRun = false;
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
            integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
            crossorigin="anonymous"
    >

    <style>
        nav a.active {
            font-weight: bold;
        }
    </style>

    <script
            src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"
    ></script>

    <script
            src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
            integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
            crossorigin="anonymous"
    ></script>


    <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
            crossorigin="anonymous"
    ></script>

    <title>2. tematická práce</title>
</head>

<body>
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal">Vyhledávání v IS STAG</h5>

    <?php include "../partials/header.php" ?>
</div>

<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Vyhledávání předmětů</h1>

    <p class="lead">
        Pomocí formuláře níže můžete dle zadaných kritérií vyhledat předměty vyučované na UJEP.
    </p>
</div>

<div class="container">
    <div class="col-md-8 ml-auto mr-auto order-md-1">
        <h4 class="mb-3">Kritéria vyhledávání</h4>

        <?php if($requestCalled && !$isSomeParamsSet): ?>
            <div class="alert alert-danger" role="alert">
                Chyba! Je potřeba specifikovat alespoň jedno vyhledávací kritérium.
            </div>
        <?php endif; ?>

        <form class="needs-validation" novalidate="" method="post" action="subjects">
            <div class="row">
                <div class="col-md-9 mb-3">
                    <label for="subjectName">Název předmětu</label>

                    <input
                            type="text"
                            class="form-control"
                            id="subjectName"
                            placeholder="Základy tvorby webových stránek B"
                            name="nazev"
                            <?=enterValueIfExists('nazev')?>
                    >
                </div>

                <div class="col-md-3 mb-3">
                    <label for="subjectShortcut">Zkratka předmětu</label>

                    <input
                            type="text"
                            class="form-control"
                            id="subjectShortcut"
                            placeholder="0196"
                            name="zkratka"
                            <?=enterValueIfExists('zkratka')?>
                    >
                </div>
            </div>

            <div class="mb-3">
                <label for="department">Katedra</label>

                <select
                        class="custom-select d-block w-100"
                        id="department"
                        name="pracoviste"
                >
                    <option value="" <?=noneIsSelected('pracoviste')?>>Zvolte katedru...</option>
                    <?php foreach (DepartureRepository::GetDepartures() as $shortcut => $name): ?>
                        <option value="<?=$shortcut?>" <?=isSelected('pracoviste', $shortcut)?>><?=$name?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="year">Rok</label>
                    <select class="custom-select d-block w-100" id="year" name="rok">
                        <option value="" <?=noneIsSelected('rok')?>>Zvolte rok...</option>
                        <?php foreach (Calendar::GetLastYears($yearsCount) as $year): ?>
                            <option <?=isSelected('rok', $year)?>><?=$year?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="col-md-6 mb-3">
                    <label for="language">Jazyk</label>

                    <select class="custom-select d-block w-100" id="language" required="" name="jazyk">
                        <option <?= noneIsSelected('jazyk')?> value="">Zvolte jazyk...</option>
                        <option <?= isSelected('jazyk', 'CS') ?>>CS</option>
                        <option <?= isSelected('jazyk', 'EN')?>>EN</option>
                    </select>
                </div>
            </div>

            <hr class="mb-4">

            <button
                    class="btn btn-primary btn-lg btn-block"
                    type="submit"
                    name="rqs_btn"
            >
                Vyhledat
            </button>
        </form>

        <?php if($requestRun && !$response->isAnyResults()): ?>
        <h4 class="mt-3 mb-3">Výsledky vyhledávání</h4>
        <div class="alert alert-warning" role="alert">
            Pro zadaná kritéria nebyly nalezeny žádné výsledky.
        </div>
        <?php elseif ($requestRun && $response->isAnyResults()): ?>
        <h4 class="mt-3 mb-3">Výsledky vyhledávání</h4>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th scope="col">Katedra</th>
                <th scope="col">Zkratka</th>
                <th scope="col">Název</th>
                <th scope="col">Rok</th>
            </tr>
            </thead>

            <tbody>
            <?php foreach ($response->getResults() as $subject): ?>
                <tr>
                    <td><?= $subject->getKatedra() ?></td>
                    <td><?= $subject->getZkratka() ?></td>
                    <td><?= $subject->getNazev() ?></td>
                    <td><?= $subject->getRok() ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php endif; ?>
    </div>

    <?php include "../partials/footer.php" ?>
</div>
</body>
</html>