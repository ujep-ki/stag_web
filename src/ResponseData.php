<?php


namespace TechnikTomCZ\StagWeb;


class ResponseData {
    private array $results;
    private bool $anyResults;

    public function __construct(array $results)
    {
        $this->results = $results;
        $this->anyResults = sizeof($results) > 0;
    }

    public function getResults(): array
    {
        return $this->results;
    }

    public function isAnyResults(): bool
    {
        return $this->anyResults;
    }
}