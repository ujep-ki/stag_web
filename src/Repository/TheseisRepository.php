<?php

namespace TechnikTomCZ\StagWeb\Repository;

use Exception;
use TechnikTomCZ\StagWeb\ApiClient;
use TechnikTomCZ\StagWeb\ResponseData;

class TheseisRepository
{

    public static function GetData(array $params): ResponseData
    {
        try {
            return new ResponseData(ApiClient::GetInstance()->findThesis($params));
        } catch (Exception $e) {
            return new ResponseData([]);
        }
    }
}