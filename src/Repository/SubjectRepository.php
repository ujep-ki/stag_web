<?php
    namespace TechnikTomCZ\StagWeb\Repository;

    use Exception;
    use TechnikTomCZ\StagWeb\ApiClient;
    use TechnikTomCZ\StagWeb\ResponseData;

    class SubjectRepository {

        public static function GetData(array $params): ResponseData
        {
            foreach (array_keys($params) as $key) {
                if('nazev' == $key) {
                    $params[$key] = "%".urlencode($params[$key])."%";
                } else {
                    $params[$key] = urlencode($params[$key]);
                }
            }

            try {
                return new ResponseData(ApiClient::GetInstance()->findSubjects($params));
            } catch (Exception $e) {
                return new ResponseData([]);
            }
        }
    }