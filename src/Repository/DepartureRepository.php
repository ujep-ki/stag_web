<?php


namespace TechnikTomCZ\StagWeb\Repository;

class DepartureRepository
{
    public static function GetDepartures()
    {
        return [
            'KI' => 'Katedra Informatiky',
            'KMA' => 'Katedra Matematiky',
            'KBI' => 'Katedra Biologie',
            'KFY' => 'Katedra Fyziky',
            'KCH' => 'Katedra Chemie'
        ];
    }
}