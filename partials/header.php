<?php
    use TechnikTomCZ\StagWeb\Nav;
    global $pages;

    $currentPage = str_replace('/', '', str_replace('/index.php', '', $_SERVER['REQUEST_URI']));

    if(strlen($currentPage) === 0) {
        $currentPage = 'home';
    }

    $nav = new Nav($pages);

    echo $nav->render($currentPage);
